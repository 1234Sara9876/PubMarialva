﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PubMarialva.Models;

namespace PubMarialva.Controllers
{
    public class HomeController : Controller
    {
        DCMarialvaDataContext dc = new DCMarialvaDataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Local()
        {
            return View();
        }

        public ActionResult Reservas()
        {
            return View();
        }

        public ActionResult Menu()
        {
            List<Menu> ementa = dc.Menu.ToList();
            return View(ementa);
        }

        [HttpPost]
        public ActionResult Reservas (Reserva marcacao)
        {
            if (ModelState.IsValid)
            {
                dc.Reserva.InsertOnSubmit(marcacao);
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}